from jbl.dep import *
import os
import shutil


class File:
	def __init__(self, path, contents: Lazy[Any]):
		self.path = path
		self._contents_node = contents
		self._exists = None

	def get_path(self):
		return self.path

	def exists(self) -> Lazy[bool]:
		if not self._exists:
			@async
			def fn(cc: ComputeContext):
				yield self.dependency(cc)
				return os.path.exists(self.path)
			self._exists = FuncNode(fn)
		return self._exists

	# materializes the file and makes it a dependency
	@async
	def dependency(self, cc: ComputeContext):
		yield cc.depends_on(self._contents_node)
		return self.require(cc)

	# materializes the file but does not make it a dependency
	def require(self, cc: ComputeContext):
		return self._contents_node.eval(cc.bc)

	def copy(self, dest: str) -> 'File':
		@async
		def copier(cc: ComputeContext):
			yield self.dependency(cc)
			print(f"Copying {self.path} to {dest}...")
			shutil.copy(self.path, dest)
			return noncachable()
		return output(dest, lazy(copier))


@functools.lru_cache(maxsize=None)
def locate(path: str) -> File:
	def get_timestamp(bc: BuildContext):
		if not os.path.exists(path):
			return None
		if os.path.isdir(path):
			return "dir"
		return "ts:" + str(os.stat(path).st_mtime)
	return File(path, InputNode(get_timestamp))


def output(path: str, builder: Lazy[Any]) -> File:
	return File(path, FuncNode(lambda cc: builder.get(cc).then(lambda _: id(object())), f"contents of {path}"))


