import subprocess
import sys


def command(args):
	print(' '.join(args))
	subprocess.run(args, check=True)
	sys.stdout.flush()
	sys.stderr.flush()


