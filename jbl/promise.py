from typing import TypeVar, Generic, Union, Tuple, Callable, Any, Optional, Generator, NewType, List
import functools
import sys
import threading


T = TypeVar('T')
U = TypeVar('U')


class PromiseRejectedException(Exception):
	def __init__(self, reason):
		Exception.__init__(self, "promise rejected")
		self.reason = reason

	def __str__(self):
		return f"promise rejected: {self.reason}"


class PromiseResolver:
	__slots__ = ("target",)

	def __init__(self, promise):
		self.target = promise

	def _chain(self, succ, value):
		if succ:
			self.resolve(value)
		else:
			self.reject(value)

	def resolve(self, value):
		if not self.target:
			return
		if isinstance(value, Promise):
			resolver = PromiseResolver(self.target)
			value._chain(resolver._chain)
		else:
			self.target._do_resolve(value)
		self.target = None

	def reject(self, value):
		if not self.target:
			return
		self.target._do_reject(value)
		self.target = None


class PromiseEventLoop:
	def __init__(self):
		self._async_errors = []
		self._queue = []
		self._lock = threading.RLock()
		self._cond = threading.Condition(self._lock)

	def lock(self):

		class LockGuard:
			def __init__(self, lock, cond):
				self.lock = lock
				self.cond = cond

			def __enter__(self):
				self.lock.acquire()

			def __exit__(self, type, value, traceback):
				self.cond.notify()
				self.lock.release()

		return LockGuard(self._lock, self._cond)

	def errors(self):
		if not self._async_errors:
			return
		errors = self._async_errors
		self._async_errors = []
		yield from errors

	def invoke_async(self, fn):
		self._queue.append(fn)

	def report_async_error(self, err):
		self._async_errors.append(err)

	def wait(self):
		with self.lock():
			if len(self._queue) == 0:
				self._cond.wait()

	def process(self):
		with self.lock():
			if len(self._queue) == 0:
				return False
			i = 0
			while True:
				n = len(self._queue)
				if i >= n:
					self._queue.clear()
					break
				if i * 2 > n:
					del self._queue[:i]
					i = 0
					n = len(self._queue)
				while i < n:
					self._queue[i]()
					i += 1
			return True


EventLoop = PromiseEventLoop()


def wrap_exception(exc):
	if isinstance(exc, BaseException):
		raise exc
	else:
		raise PromiseRejectedException(exc)


def unwrap_exception(exc):
	if isinstance(exc, PromiseRejectedException):
		return exc.reason
	else:
		return exc


class Promise(Generic[T]):
	__NoCallbackType = NewType("NoCallbackType", object)
	__NoCallback = __NoCallbackType(object())

	__StatePending = 0
	__StateResolved = 1
	__StateRejected = 2

	__slots__ = ("_state", "_value", "_queue")

	def __init__(self, fn: Union[__NoCallbackType, Callable[[Callable[[T], None], Callable[[Any], None]], None]]):
		self._state = Promise.__StatePending
		self._value = None
		self._queue = None
		if fn != Promise.__NoCallback:
			def run():
				resolver = PromiseResolver(self)
				try:
					fn(resolver.resolve, resolver.reject)
				except:
					reason = sys.exc_info()[1]
					resolver.reject(reason)
			EventLoop.invoke_async(run)

	def _enqueue(self):
		EventLoop.invoke_async(self._dispatch)

	def _dispatch(self):
		if self._queue:
			for cb in self._queue:
				cb(self._state == Promise.__StateResolved, self._value)
			self._queue = None
		elif self._state == Promise.__StateRejected:
			# Report async reject
			EventLoop.report_async_error(self._value)

	def _chain(self, fn):
		if self._queue:
			self._queue.append(fn)
		else:
			if self._queue is None:
				self._queue = [fn]
			else:
				self._queue.append(fn)
			if self._state != Promise.__StatePending:
				self._enqueue()

	def then(
		self,
		on_resolved: Callable[[T], Union['Promise[U]', U]],
		on_rejected: Optional[Callable[[Any], Union['Promise[U]', U]]] = None
	) -> 'Promise[U]':
		promise, resolve, reject = Promise.defer()

		def fn(succ, val):
			try:
				if succ:
					resolve(on_resolved(val))
				else:
					if on_rejected is not None:
						resolve(on_rejected(val))
					else:
						reject(val)
			except:
				reject(sys.exc_info()[1])

		self._chain(fn)
		return promise

	def finally_(self, on_settled: Callable[[], Union[U, 'Promise[U]']]) -> 'Promise[U]':
		return self.then(lambda _: on_settled(), lambda _: on_settled())

	@staticmethod
	def defer()->Tuple['Promise[T]', Callable[[T], None], Callable[[Any], None]]:
		promise = Promise(Promise.__NoCallback)
		resolver = PromiseResolver(promise)
		return promise, resolver.resolve, resolver.reject

	@staticmethod
	def resolve(value: Union[T, 'Promise[T]']) -> 'Promise[T]':
		if isinstance(value, Promise):
			return value
		else:
			promise = Promise(Promise.__NoCallback)
			promise._do_resolve(value)
			return promise

	@staticmethod
	def reject(reason: Any) -> 'Promise[T]':
		promise = Promise(Promise.__NoCallback)
		promise._do_reject(reason)
		return promise

	@staticmethod
	def all(*promises: Any) -> 'Promise[List[Any]]':
		if len(promises) == 0:
			return Promise.resolve([])
		else:
			result, resolve, reject = Promise.defer()
			results = []
			count = len(promises)

			for idx, promise in enumerate(promises):
				results.append(None)

				def on_resolve(val, idx=idx):
					nonlocal count
					results[idx] = val
					count -= 1
					if count <= 0:
						resolve(results)

				def on_reject(val):
					reject(val)

				if isinstance(promise, Promise):
					promise.then(on_resolve, on_reject)
				else:
					on_resolve(promise, idx)

			return result

	def join(self):
		going = True
		success = None
		result = None

		def on_completed(s, r):
			nonlocal success, result, going
			going = False
			success = s
			result = r

		self._chain(on_completed)

		while going:
			if not EventLoop.process():
				EventLoop.wait()

		if success:
			return result

		raise wrap_exception(result)

	def _enqueue_if_needed(self):
		if self._queue:
			self._enqueue()

	def _do_resolve(self, value):
		self._state = Promise.__StateResolved
		self._value = value
		self._enqueue_if_needed()

	def _do_reject(self, reason):
		self._state = Promise.__StateRejected
		self._value = unwrap_exception(reason)
		self._enqueue()


def promisify(value: Union[T, Promise[T]]) -> Promise[T]:
	return Promise.resolve(value)


def async(fn: Callable[[Any], Generator[Promise[Any], Any, T]]) -> Callable[[Any], Promise[T]]:
	@functools.wraps(fn)
	def wrapper(*args, **kwargs):

		def run(resolve, reject):
			iterator = fn(*args, **kwargs)

			def spin(success, value):
				try:
					if success:
						k = iterator.send(value)
					else:
						k = iterator.throw(wrap_exception(value))
					promisify(k)._chain(lambda s, v: spin(s, v))
				except StopIteration as stop:
					return resolve(stop.value)
				except:
					return reject(sys.exc_info()[1])

			return spin(True, None)

		return Promise(run)

	return wrapper
