from jbl.promise import *
from jbl.dep import *
import jbl.file as file
import jbl.io as io
import shutil
import os
import subprocess

# Parameters -- named nodes that are to be provided by the user
build_path = parameter("build", "build")
out_name = parameter("out", "a.exe")
compiler = parameter("compiler", "D:/Soft/LLVM/bin/clang")
sources = parameter("sources", ("src/foo.c", "src/bar.c", "src/qax.c"))


# Nodes aren't values -- you cannot directly manipulate with the parameters as if they were strings
# Instead, lazy_apply is a convenience wrapper to apply a function to a bunch of lazy nodes
# First, it evaluates all the nodes given as 2nd, 3rd ... arguments.
# Then, it applies the function to the values of the nodes.
out_path = lazy_apply(lambda base: os.path.join(base, "bin"), build_path)
objs_path = lazy_apply(lambda base: os.path.join(base, "obj"), build_path)

# A function that compiles an object file, given a source file.
# As the object file's name depends on the environment (build_path),
# the function cannot return a "raw" File object.
# Instead, it wraps it within a computational node.
def compile_obj(source: file.File) -> Lazy[file.File]:
	# @async converts a function to a coroutine (use yield kwd to talk to other coroutines)
	@async
	@functools.wraps(compile_obj)
	def do_work(cc: ComputeContext):
		source_path = source.path
		# node.get(cc) evalautes the node asynchronously, so the function must yield to
		# get the actual value
		objs_path_value = yield objs_path.get(cc)
		obj_path = os.path.join(objs_path_value, source_path + ".o")

		# the builder object, "what needs to be done to construct a file"
		@async
		def obj_builder(cc: ComputeContext):
			# first we build a command
			dep_path = os.path.join(objs_path_value, source_path + ".o.dep")
			command = [
				(yield compiler.get(cc)),
				source_path,
				"-MMD", "-MF", dep_path,
				"-c",
				"-o", obj_path
			]
			# declare dependency on the source file
			# this function forces source file builder (if any)
			yield source.dependency(cc)
			# executes the command
			io.command(command)
			# returning noncachable() prevents this builder from being cached
			# you may return a hash of the output, or some other meaningful state of your builder
			# if you do, then any computation depending on the builder will not be
			# recomputed if the builder returns the same token
			return noncachable()
		# file.output creates a file object with the given path and the builder
		# the file will not be built right away!
		return file.output(obj_path, lazy(obj_builder))
	# wrap do_work in a computational node
	return lazy(do_work)

# translates source file names to file objects
def sources_to_files(names):
	return tuple(map(file.locate, names))

# source file objects
source_files = lazy_apply(sources_to_files, sources)

# compiles all the sources and return a tuple containing all the objects
@async
def compile_objects(cc: ComputeContext):
	objects = []
	source_list = yield source_files.get(cc)
	# this is crucial: fork establishes that all prior dependencies must be evaluated
	# before proceeding
	# as source_list depends on source_files, and we use source_list later on,
	# we must use fork(), or incremental rebuild will still recompile old objects
	# in the actual build system language we will statically eliminate the need for fork()
	# with the flow analysis
	cc = cc.fork()
	for source in source_list:
		object = yield compile_obj(source).get(cc)
		objects.append(object)
	# it is very important to keep all exposed values in computational nodes immutable
	# otherwise, the results may be unpredictable
	return tuple(objects)


# linking exe is mostly very similar to compile_obj
def link_exe(objects: Tuple[file.File], name: str) -> Lazy[file.File]:
	@async
	@functools.wraps(link_exe)
	def do_work(cc: ComputeContext):
		out_path_value = yield out_path.get(cc)
		exe_path = os.path.join(out_path_value, name)

		@async
		def exe_builder(cc: ComputeContext):
			obj_paths = []
			for obj in objects:
				obj_paths.append(obj.path)
				obj.require(cc)
			for obj in objects:
				yield obj.dependency(cc)
			command = [
				(yield compiler.get(cc)),
				*obj_paths,
				"-o", exe_path
			]
			io.command(command)
			return noncachable()

		return file.output(exe_path, lazy(exe_builder))
	return lazy(do_work)

# all the object files (of type Lazy[Tuple[file.File]])
objects = lazy(compile_objects)

# final exe (of type Lazy[file.File])
exe = lazy_apply(link_exe, objects, out_name)

# this is needed to force evaluation of exe contents
@async
def eval_exe(cc):
	exe_file = yield exe.get(cc)
	cc = cc.fork()
	yield exe_file.dependency(cc)
	return noncachable()


main = lazy(eval_exe)


def build(node):
	bc = BuildContext()
	print("==== BUILD STARTED ====")
	try:
		result = node.eval(bc).join()
		print("BUILD SUCCEDED")
		print(f"RESULT: {result}")
	except:
		print("BUILD FAILED!")
		reason = sys.exc_info()[1]
		print(reason)
	print("=== BUILD FINISHED ===")


build(main)
print("Changing source list to except foo.c from the dependencies...")
sources.set_value(("src/bar.c", "src/qax.c"))
build(main)
input("Please change one (or more) of the files and press enter...")
build(main)
