from typing import Callable, TypeVar
from jbl.promise import *
from abc import abstractmethod
from inspect import getmembers

T = TypeVar('T')


class BuildContext:
	def __init__(self):
		pass


class BuildError:
	def __init__(self, reason, frame, parent = None):
		self.reason = reason
		self.frame = frame
		self.parent = parent

	def __str__(self):
		return str(self.reason) + "\nStack Trace:\n" + ''.join(map(lambda s: "\t" + str(s) + "\n", self._iter_frames()))

	def _iter_frames(self):
		yield self.frame
		if self.parent:
			yield from self.parent._iter_frames()


class ValueCache:
	def __init__(self, deps: List[Tuple['ValueNode[Any]', Any]], children: List['ValueCache']):
		self.deps = deps
		self.children = children

	def verify(self, bc: BuildContext) -> Promise[bool]:
		if len(self.deps) > 0:
			promises = []
			for dep, token in self.deps:
				promises.append(dep.verify(bc, token))
			return Promise.all(*promises).then(lambda _: self._verify_children(bc))
		else:
			return self._verify_children(bc)

	def _verify_children(self, bc: BuildContext) -> Promise[bool]:
		if len(self.children) > 0:
			promises = []
			for child in self.children:
				promises.append(child.verify(bc))
			return Promise.all(promises).then(lambda _: True)
		else:
			return promisify(True)


class ComputeContext:
	def __init__(self, bc: BuildContext):
		self.bc = bc
		self.deps = None
		self.children = None

	def depends_on(self, node: 'ValueNode[T]'):
		if self.deps is None:
			self.deps = set()
		elif node in self.deps:
			return
		self.deps.add(node)

	def get(self, val):
		if isinstance(val, ValueNode):
			return val.get(self)
		else:
			return constant(val).get(self)

	def fork(self)->'ComputeContext':
		child = ComputeContext(self.bc)
		if not self.children:
			self.children = []
		self.children.append(child)
		return child

	def to_value_cache(self, all_tokens: List[Promise[Any]]) -> ValueCache:
		deps = []

		def suppress(_):
			pass

		if self.deps is not None:
			for dep in self.deps:
				token = dep.capture(self.bc)

				def on_token_ready(token_value, dep=dep):
					deps.append((dep, token_value))

				all_tokens.append(token.then(
					on_token_ready,
					suppress
				))
		children = []
		if self.children is not None:
			for child in self.children:
				children.append(child.to_value_cache(all_tokens))
		return ValueCache(deps, children)

	def capture(self) -> Promise[ValueCache]:
		tokens = []
		value_cache = self.to_value_cache(tokens)
		return Promise.all(*tokens).then(lambda _: value_cache)


class ValueNode(Generic[T]):
	def get(self, cc: ComputeContext) -> Promise[T]:
		cc.depends_on(self)
		return self.eval(cc.bc)

	def peek(self, cc: ComputeContext) -> Promise[T]:
		return self.eval(cc.bc)

	@abstractmethod
	def eval(self, bc: BuildContext) -> Promise[T]:
		return self.eval(bc)

	def capture(self, bc: BuildContext) -> Promise[Any]:
		return self.eval(bc)

	def verify(self, bc: BuildContext, token: Any) -> Promise[bool]:
		def on_value(val):
			if val == token:
				return True
			return Promise.reject(repr(self) + " has changed")

		return self.eval(bc).then(on_value)

	def log(self, message):
		#print(f"{str(self)}@{id(self)}: {message}")
		pass


Lazy = ValueNode


class ConstantNode(ValueNode):
	def __init__(self, value):
		self.value = value
		self.value_wrapper = Promise.resolve(value)

	def eval(self, bc):
		return self.value_wrapper

	def __repr__(self):
		return f"constant({self.value})"


@functools.lru_cache(maxsize=None)
def constant(value):
	return ConstantNode(value)


class ParameterNode(ValueNode):
	def __init__(self, name, value=None):
		self.name = name
		self.value = value
		self.value_wrapper = None

	def set_value(self, value):
		if value != self.value:
			self.value = value
			self.value_wrapper = None

	def get_value(self):
		return self.value

	def eval(self, bc):
		if self.value_wrapper is None:
			self.value_wrapper = Promise.resolve(self.value)
		return self.value_wrapper

	def __repr__(self):
		return f"parameter({self.name})"


class InputNode(ValueNode):
	def __init__(self, fn):
		self.last_computed_bc = None
		self.fn = fn
		self.value = None

	def eval(self, bc: BuildContext)->Promise[T]:
		if self.last_computed_bc == bc:
			return self.value
		self.value = self._compute_next(bc)
		self.last_computed_bc = bc
		return self.value

	def _compute_next(self, bc: BuildContext)->Promise[T]:
		self.log(f"input requested")
		return self.compute(bc)

	def compute(self, bc: BuildContext) -> Promise[T]:
		return Promise(lambda res, rej: promisify(self.fn(bc)).then(res, rej))


class ComputableNode(ValueNode):
	def __init__(self):
		self.last_computed_bc = None
		self.value = None
		self.cached_value = None
		self.cache = None

	def eval(self, bc: BuildContext)->Promise[T]:
		if self.last_computed_bc == bc:
			return self.value
		self.value = self._compute_next(bc)
		self.last_computed_bc = bc
		return self.value

	def _compute_next(self, bc: BuildContext)->Promise[T]:
		self.log(f"computation requested")
		if self.cache is not None:
			self.log("has cache, attempts to validate...")

			def on_cache_valid(_):
				self.log(f"validation success")
				return self.cached_value

			def on_cache_fail(reason):
				self.log(f"validation failed: {reason}")
				return self._run_compute(bc)

			return self.cache.verify(bc).then(
				on_cache_valid,
				on_cache_fail
			)
		else:
			self.log(f"no cache available")
			return self._run_compute(bc)

	def _run_compute(self, bc: BuildContext) -> Promise[T]:
		cc = ComputeContext(bc)

		def on_result(val):
			self.log(f"computation completed")

			def update_cache(cache):
				self.cache = cache
				self.cached_value = val
				return val

			return cc.capture().then(update_cache)

		def on_fail(reason):
			if isinstance(reason, BuildError):
				err = BuildError(reason.reason, self, reason)
			else:
				err = BuildError(reason, self, None)
			self.log(f"computation failed: {err.reason}")
			return Promise.reject(err)

		self.log(f"starting computation...")
		return self.compute(cc).then(on_result, on_fail)

	@abstractmethod
	def compute(self, cc: ComputeContext)->Promise[T]:
		pass


def identify(object):
	if hasattr(object, '__call__'):
		return get_fn_token(object)
	else:
		return object


def get_fn_token(fn):
	code = fn.__code__
	if fn.__closure__:
		closure = tuple(identify(cell.cell_contents) for cell in fn.__closure__)
	else:
		closure = None
	if fn.__defaults__:
		defaults = tuple(map(identify, fn.__defaults__))
	else:
		defaults = None
	if fn.__kwdefaults__:
		kwdefaults = tuple(map((lambda xs: (xs[0], identify(xs[1]))), fn.__kwdefaults__.items()))
	else:
		kwdefaults = None
	return (code, closure, defaults, kwdefaults)


class FuncNode(ComputableNode):
	def __init__(self, fn, name=None):
		ComputableNode.__init__(self)
		self.fn = fn
		self.name = name if name is not None else self.fn.__name__

	def compute(self, cc: ComputeContext) -> Promise[T]:
		return Promise(lambda res, rej: promisify(self.fn(cc)).then(res, rej))

	def __repr__(self):
		return f"function({self.name})"


_NodeCache = dict()


def lazy(fn, name=None):
	token = get_fn_token(fn)
	if token in _NodeCache:
		return _NodeCache[token]
	else:
		node = FuncNode(fn, name)
		_NodeCache[token] = node
		return node


def lazy_apply(fn, *args):
	@async
	@functools.wraps(fn)
	def wrapper(cc: ComputeContext):
		val_args = []
		for arg in args:
			arg_value = yield cc.get(arg)
			val_args.append(arg_value)
		return (yield cc.get(fn(*val_args)))

	return lazy(wrapper)


def parameter(name, value):
	return ParameterNode(name, value)


def noncachable():
	return id(object())
